# kimmo-icon-theme
The Kimmo Icon theme.

The Kimmo Icon Theme is a mixture of the Arc/Numix theme,
colored orange, looking great on all desktops. The theme has an
associated gtk theme as well: kimmo-gtk-theme

At the moment, the Kimmo GTK Theme is undergoing revisions and
is currently not available yet.
